/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for AccountDetailExt.
 */
@isTest
private class TestAccountDetailExt
{
    static testMethod void testCase1()
    {
        Account accObj = new Account();
        accObj = DataGenerator.accountInsert();
        ApexPages.StandardController apexObj = new ApexPages.StandardController (accObj);

        List<User_Preference__c> tempUserPref = new List<User_Preference__c>();
        tempUserPref = [select Name,SelectedTab__c from User_Preference__c where User__C=:UserInfo.getUserId() Limit 1];
        if(tempUserPref.size() > 0 ) {
            tempUserPref[0].SelectedTab__c =1;
            update tempUserPref;
        }else{
            User_Preference__c newUserPref = new User_Preference__c();
            newUserPref.Name =  UserInfo.getUserName();
            newUserPref.User__c = UserInfo.getUserId();
            newUserPref.SelectedTab__c = 1;
            insert newUserPref;
        }

        Test.startTest();
        AccountDetailExt accDetailObj = new AccountDetailExt(apexObj);
        System.assertEquals(accDetailObj.selectedTab,'accountTab');
        
        accDetailObj.chackAndRedirect();
        accDetailObj.getTabPreference(tempUserPref );
        Test.stopTest();
    }
    static testMethod void testCase2()
    {
        Account accObj = new Account();
        accObj = DataGenerator.accountInsert();
        ApexPages.StandardController apexObj = new ApexPages.StandardController (accObj);

        List<User_Preference__c> tempUserPref = new List<User_Preference__c>();
        tempUserPref = [select Name,SelectedTab__c,Id from User_Preference__c where User__C=:UserInfo.getUserId() Limit 1];
        if(tempUserPref.size() > 0 ) {
            tempUserPref[0].SelectedTab__c =2;
            update tempUserPref;
        }else{
            User_Preference__c newUserPref = new User_Preference__c();
            newUserPref.Name =  UserInfo.getUserName();
            newUserPref.User__c = UserInfo.getUserId();
            newUserPref.SelectedTab__c = 2;
            insert newUserPref;
        }

        Test.startTest();

        AccountDetailExt accDetailObj1 = new AccountDetailExt(apexObj);
        System.assertEquals(accDetailObj1.selectedTab,'txTab');

        Test.stopTest();
    }
    static testMethod void testCase3()
    {
        Account accObj = new Account();
        accObj = DataGenerator.accountInsert();
        ApexPages.StandardController apexObj = new ApexPages.StandardController (accObj);

        List<User_Preference__c> tempUserPref = new List<User_Preference__c>();
        tempUserPref = [select Name,SelectedTab__c from User_Preference__c where User__C=:UserInfo.getUserId() Limit 1];
        if(tempUserPref.size() > 0 ) {
            tempUserPref[0].SelectedTab__c =3;
            update tempUserPref;
        }else{
            User_Preference__c newUserPref = new User_Preference__c();
            newUserPref.Name =  UserInfo.getUserName();
            newUserPref.User__c = UserInfo.getUserId();
            newUserPref.SelectedTab__c = 3;
            insert newUserPref;
        }

        Test.startTest();

        AccountDetailExt accDetailObj1 = new AccountDetailExt(apexObj);
        System.assertEquals(accDetailObj1.selectedTab,'disputesTab');

        Test.stopTest();
    }

    static testMethod void testCase4()
    {
        Account accObj = new Account();
        accObj = DataGenerator.accountInsert();
        ApexPages.StandardController apexObj = new ApexPages.StandardController (accObj);

        List<User_Preference__c> tempUserPref = new List<User_Preference__c>();
        tempUserPref = [select Name,SelectedTab__c from User_Preference__c where User__C=:UserInfo.getUserId() Limit 1];
        if(tempUserPref.size() > 0 ) {
            tempUserPref[0].SelectedTab__c =4;
            update tempUserPref;
        }else{
            User_Preference__c newUserPref = new User_Preference__c();
            newUserPref.Name =  UserInfo.getUserName();
            newUserPref.User__c = UserInfo.getUserId();
            newUserPref.SelectedTab__c = 4;
            insert newUserPref;
        }

        Test.startTest();

        AccountDetailExt accDetailObj1 = new AccountDetailExt(apexObj);
        System.assertEquals(accDetailObj1.selectedTab,'notesTab');

        accDetailObj1.getTabIndex();

        Test.stopTest();
    }
    
    static testMethod void testCase5()
    {
        Account accObj1 = new Account();
        accObj1 = accountInsertonce();
        ApexPages.StandardController apexObj1 = new ApexPages.StandardController (accObj1);
        ApexPages.currentPage().getParameters().put('tabsel','1');


        List<User_Preference__c> tempUserPref = new List<User_Preference__c>();
        tempUserPref = [select Name,SelectedTab__c from User_Preference__c where User__C=:UserInfo.getUserId() Limit 1];
        if(tempUserPref.size() > 0 ) {
            tempUserPref[0].SelectedTab__c =4;
            update tempUserPref;
        }else{
            User_Preference__c newUserPref = new User_Preference__c();
            newUserPref.Name =  UserInfo.getUserName();
            newUserPref.User__c = UserInfo.getUserId();
            newUserPref.SelectedTab__c = 4;
            insert newUserPref;
        }

        Test.startTest();
        If (apexObj1 != null){
            AccountDetailExt accDetailObj5 = new AccountDetailExt(apexObj1);
            System.assertEquals(accDetailObj5.selectedTab,'notesTab');
            accDetailObj5.getTabIndex();
            accDetailObj5.chackAndRedirect();
        }  
       
        Test.stopTest();
    }
    
     static testMethod void testCase6()
    {
        Account accObj1 = new Account();
        accObj1 = accountInsertonce();
        ApexPages.StandardController apexObj1 = new ApexPages.StandardController (accObj1);
        ApexPages.currentPage().getParameters().put('tabsel','2');


        List<User_Preference__c> tempUserPref = new List<User_Preference__c>();
        tempUserPref = [select Name,SelectedTab__c from User_Preference__c where User__C=:UserInfo.getUserId() Limit 1];
        if(tempUserPref.size() > 0 ) {
            tempUserPref[0].SelectedTab__c =2;
            update tempUserPref;
        }else{
            User_Preference__c newUserPref = new User_Preference__c();
            newUserPref.Name =  UserInfo.getUserName();
            newUserPref.User__c = UserInfo.getUserId();
            newUserPref.SelectedTab__c = 2;
            insert newUserPref;
            tempUserPref.add(newUserPref);
        }

        Test.startTest();
        If (apexObj1 != null){
            AccountDetailExt accDetailObj5 = new AccountDetailExt(apexObj1);
            System.assertEquals(accDetailObj5.selectedTab,'txTab');
            accDetailObj5.getTabIndex();
            accDetailObj5.getTabPreference(tempUserPref);
        }  
       
        Test.stopTest();
    }
    
     static testMethod void testCase7()
    {
        Account accObj1 = new Account();
        accObj1 = accountInsertonce();
        ApexPages.StandardController apexObj1 = new ApexPages.StandardController (accObj1);
        ApexPages.currentPage().getParameters().put('tabsel','3');


        List<User_Preference__c> tempUserPref = new List<User_Preference__c>();
        tempUserPref = [select Name,SelectedTab__c from User_Preference__c where User__C=:UserInfo.getUserId() Limit 1];
        if(tempUserPref.size() > 0 ) {
            tempUserPref[0].SelectedTab__c =3;
            update tempUserPref;
        }else{
            User_Preference__c newUserPref = new User_Preference__c();
            newUserPref.Name =  UserInfo.getUserName();
            newUserPref.User__c = UserInfo.getUserId();
            newUserPref.SelectedTab__c = 3;
            insert newUserPref;
            tempUserPref.add(newUserPref);
        }

        Test.startTest();
        If (apexObj1 != null){
            AccountDetailExt accDetailObj5 = new AccountDetailExt(apexObj1);
            System.assertEquals(accDetailObj5.selectedTab,'disputesTab');
            accDetailObj5.getTabIndex();
            accDetailObj5.getTabPreference(tempUserPref);
        }  
       
        Test.stopTest();
    }
    
    static testMethod void testCase8()
    {
        Account accObj1 = new Account();
        accObj1 = accountInsertonce();
        ApexPages.StandardController apexObj1 = new ApexPages.StandardController (accObj1);
        ApexPages.currentPage().getParameters().put('tabsel','4');


        List<User_Preference__c> tempUserPref = new List<User_Preference__c>();
        tempUserPref = [select Name,SelectedTab__c from User_Preference__c where User__C=:UserInfo.getUserId() Limit 1];
        if(tempUserPref.size() > 0 ) {
            tempUserPref[0].SelectedTab__c =4;
            update tempUserPref;
        }else{
            User_Preference__c newUserPref = new User_Preference__c();
            newUserPref.Name =  UserInfo.getUserName();
            newUserPref.User__c = UserInfo.getUserId();
            newUserPref.SelectedTab__c = 4;
            insert newUserPref;
            tempUserPref.add(newUserPref);
        }

        Test.startTest();
        If (apexObj1 != null){
            AccountDetailExt accDetailObj5 = new AccountDetailExt(apexObj1);
            System.assertEquals(accDetailObj5.selectedTab,'notesTab');
            accDetailObj5.getTabIndex();
            accDetailObj5.getTabPreference(tempUserPref);
        }  
       
        Test.stopTest();
    }
    
    
    
    public static Account accountInsertonce()
    {
        //create an Account
        Account accObj = new Account(Name='DemoAccount');
        accObj.Account_Key__c =
                ''+(Math.random()* 1000000).intValue() + (Math.random()* 1000000).intValue();
        accObj.ownerId = UserInfo.getUserId();
        accObj.Sticky_Note__c = '';
        accObj.Sticky_Note_Level__c = 'Normal';
        insert accObj;
        return accObj;
    }
}