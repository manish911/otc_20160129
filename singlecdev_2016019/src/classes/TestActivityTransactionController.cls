/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for ActivityTransactionController.
 */

@isTest
private class TestActivityTransactionController {

    static testMethod void TestActivityTransactionController() {
        // TO DO: implement unit test

        Account tempAccount = DataGenerator.accountInsert();
        Task t = DataGenerator.taskInsert( tempAccount );
        List<Transaction__c> txList = new List<Transaction__c>();
        txList = DataGenerator.transactionInsert(tempAccount);

        List<Activity_Transaction__c> acttxList = new List<Activity_Transaction__c>();
        acttxList = DataGenerator.activityTransactionInsert(tempAccount, t);

        ApexPages.StandardController taskController = new ApexPages.StandardController( t );

        //start test
        Test.startTest();

        ActivityTransactionController acttxcontroller = new ActivityTransactionController( taskController );
        //
        //
        System.assertEquals(tempAccount.Id,acttxcontroller.accountId);

        //end test
        Test.stopTest();

    }
}