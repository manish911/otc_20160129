@isTest
/*
* MK-20160129 15.26 pm: Testing version control
*/
private class Test_New_RND_Email
{
    static testMethod void testCase()
    {
        Account a=new Account();
        test.startTest();
        
        RnDEmailController controller=new RnDEmailController();
        controller.email='abc.ed@sftpl.com';
        controller.accountId=a.Id;        
        controller.sendPdf();
        
        test.stopTest();
    }
}