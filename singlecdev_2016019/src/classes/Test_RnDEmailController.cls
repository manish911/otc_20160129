@isTest
private class Test_RnDEmailController {

  static Account account;

  static {

    account = new Account();
    account.Name = 'Test Account';
    insert account;

  }

  static testMethod void testPdfEmailer() {

    PageReference pref = Page.RnD_mail;
    pref.getParameters().put('id',account.id);
    Test.setCurrentPage(pref);

    RnDEmailController con = new RnDEmailController();    

    Test.startTest();

    System.assertEquals(2,con.accounts.size());

    // populate the field with values
    con.accountId = account.id;
    con.email = 'test@noemail.com';
    // submit the record
    pref = con.sendPdf();

    Test.stopTest(); 

  }
}